import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.nio.channels.AsynchronousByteChannel;
//for date
import java.util.*;

import javax.swing.*;


public class GAProgram 
{
	
	public static void main(String args[])
	{
	
		//Initializing Variables
		
		//Display
		System.out.println("Welcome to the Genetic Algorithm Test Bed!");
		System.out.println("Please enter a word or phrase for the algorithm to find! (Only Letters Please)");
		
		//Create scanner to grab input
		Scanner SC = new Scanner(System.in);
		
		String AlphaWord = SC.nextLine();
		
		System.out.println("Alright, I will now perform an algorithm to find "+AlphaWord);
		System.out.println("Generating Starting Pool...");
		
		//Generate random strings that are no greater than the length of test word
		int maxLS = AlphaWord.length();

		int Generations = 0;
		
		Date date = new Date();
		String starttime = String.format("%tc", date);
		
		
		
		List<String> RandomWords = GetRandomStrings(maxLS,125*maxLS);
		
		//Changed to getsortedrankseff instead of just getsortedranks
		List<String> Sorted = GetSortedRanksEff(RandomWords,AlphaWord);
		
		List<String> HighestScored = new LinkedList<String>();
		
		int BestScore = 0;
		String BestScoredWord = GetRandomStrings(AlphaWord.length(), 1).get(0);
		String SecondBestScoredWord = GetRandomStrings(AlphaWord.length(), 1).get(0);
		String ThirdBestScoredWord = GetRandomStrings(AlphaWord.length(), 1).get(0);
		
		Random R = new Random(System.currentTimeMillis());
		
		//JFrame Stuff+
		JFrame JF = new JFrame();
		
		//Set the frame size for jframe
		JF.setSize(new Dimension(600,200));
			
		//set start position of frame to middle
		JF.setLocationRelativeTo(null);
			
		//Set a default close action
		JF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					
		//Name of jframe up at the top
		JF.setTitle("Gentic Algorithm Word Solver: By John Dodd");
			
		//No resizing!!
		JF.setResizable(true);
		
		//Set visible
		JF.setVisible(true);
		
		JPanel JP = new JPanel();
		
		JP.setLayout(new BoxLayout (JP, BoxLayout.Y_AXIS));
		
		JLabel JL1 = new JLabel("default");
		JLabel JL2 = new JLabel("default");
		JLabel JL3 = new JLabel("default");
		JLabel JL4 = new JLabel("default");
		
		JL1.setFont(new Font(JL1.getFont().getName(),Font.PLAIN,28));
		JL2.setFont(new Font(JL2.getFont().getName(),Font.PLAIN,28));
		JL3.setFont(new Font(JL3.getFont().getName(),Font.PLAIN,28));
		JL4.setFont(new Font(JL4.getFont().getName(),Font.PLAIN,28));
		
		//Add all components
		
		JP.add(JL1);
		JP.add(JL2);
		JP.add(JL3);
		JP.add(JL4);
		
		
		JF.add(JP);
		
		
		//Main Loop
		while(true)
		{
						
			//If this is the best match so far set best scored word to this word and set new higher standard
			if (RankWord(AlphaWord,Sorted.get(Sorted.size()-1)) > BestScore)
			{
				BestScore = RankWord(AlphaWord,Sorted.get(Sorted.size()-1));
				
				HighestScored.add(BestScoredWord);
				
				BestScoredWord = Sorted.get(Sorted.size()-1);
			}

			SecondBestScoredWord = Sorted.get(Sorted.size()-2);
			ThirdBestScoredWord = Sorted.get(Sorted.size()-3);
			
			
			JL1.setText("Best Match: "+BestScoredWord);
			JL2.setText("Second: "+Sorted.get(Sorted.size()-2));
			JL3.setText("Third: "+Sorted.get(Sorted.size()-3));
			

			//System.out.println("Random Words Size: "+RandomWords.size());
			System.out.println("Best Match? "+BestScoredWord);
			System.out.println("Second: "+Sorted.get(Sorted.size()-2));
			System.out.println("Third: "+Sorted.get(Sorted.size()-3));					
			
			
			//Breakout once word is found			
			if (AlphaWord.equals(Sorted.get(Sorted.size()-1)))
			{
				break;
			}
							
			
			//Pull first 5 top words off of lists and add to randomwords
			RandomWords = GetSimilarStrings(maxLS,5,Sorted.get(Sorted.size()-1));
			
			
			//REMOVED: Actually increases time for solve by increasing randomwords size each generation
			//randomize for all highest scored
			//for(String x : HighestScored)
			//{
			//	RandomWords.addAll(GetSimilarStrings(maxLS,5,x));
			//}
			
			//Add some dissimilar strings aswell just for genetic variety
			for(int i = 0; i < 5; i++)
			{
				RandomWords.addAll(GetSimilarStrings(maxLS,25,Sorted.get(i)));
			}
			
			//Second best third mixing 550 is best setting so far
			//75 for fast gens
			//125 sorts at 50 char count so ratio that...2.5 times char count
			for (int j = 0; j < (AlphaWord.length() * 2.5); j++)
			{
				
			String Merger = "";
			//Merge first second and third best guesses randomly
			for(int i = 0; i < BestScoredWord.length(); i++)
			{
					//Merger+= BestScoredWord.charAt(i);
				//Best at 12
				int rando = R.nextInt(12);
				
				if (rando == 0)
				{

					//Random from anywhere in list! :D
					Merger+=Sorted.get(Sorted.size()-1-R.nextInt(Sorted.size()-1)).charAt(i);

				}
				else if (rando == 1)
				{
					Merger+= SecondBestScoredWord.charAt(i);
				}
				//REMOVED: Doesn't work with larger words, deteriorates them to giberish
				//else if (rando == 2)
				//{
				//	Merger+= BestScoredWord.charAt(i);
				//}
				else
				{
					Merger+= ThirdBestScoredWord.charAt(i);
				}
				
				
			}
			
			
			RandomWords.add(Merger);
			
			
			}
			
			//System.out.println("Merger: "+Merger);
			
			//RandomWords.addAll(GetSimilarStrings(maxLS,100,Merger));
			

			
			String temp1 = "";
			String temp2 = "";
			//Take first part of best scored, and second part of second best and mix and match
			for(int i = 0; i < AlphaWord.length(); i++)
			{
				
				if (i < AlphaWord.length()/2)
				{
					temp1 += ThirdBestScoredWord.charAt(i);
					temp2 += SecondBestScoredWord.charAt(i);
				}
				else
				{
					temp2 += ThirdBestScoredWord.charAt(i);
					temp1 += SecondBestScoredWord.charAt(i);
				}
				
			}
			
			
			//System.out.println(temp1);
			//System.out.println(temp2);
			
			
			RandomWords.add(BestScoredWord);
			//RandomWords.addAll(GetSimilarStrings(maxLS,100,BestScoredWord));
			RandomWords.add(temp1);
			RandomWords.add(temp2);
			
			
			
			
			Sorted = GetSortedRanks(RandomWords,AlphaWord);
			
			
			Generations++;
			
			JL4.setText("Generation: "+Generations+ "			");
			
			System.out.println("Generation: "+Generations);
			
			
		}
		
		System.out.println("MATCH FOUND: "+Sorted.get(Sorted.size()-1));
		System.out.println("Generation: "+Generations);
		System.out.println("Start Time: "+starttime);
		Date enddate = new Date();
		System.out.println("End Time:  "+String.format("%tc", enddate));
		
		JL1.setText("MATCH FOUND: "+Sorted.get(Sorted.size()-1));
		JL2.setText("Generation: "+Generations);
		JL3.setText("Start Time: "+starttime);
		JL4.setText("End Time:  "+String.format("%tc", enddate));
		
	}
	

	public static List<String> GetRandomStrings(int length, int amount)
	{
				
		String ABC = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUV .,?!'";
		
		Random R = new Random(System.currentTimeMillis());
		
		String temp = "";
		
		List<String> Words = new LinkedList<String>();
		
		//Number of words
		for(int i = 0; i < amount; i++)
		{
			while(temp.length() < length)
			{
				int ran = R.nextInt(ABC.length());
			
				temp += ABC.charAt(ran);
			}
			
			Words.add(temp);
			temp = "";
			
		}
		
		
		
		return Words;
	}
	
	public static int RankWord(String CompareTo, String Rankees)
	{
		int score = 0;
		
		if (Rankees.length() == CompareTo.length())
		{
			for(int i = 0; i < CompareTo.length(); i++)
			{
				if (Rankees.charAt(i) == CompareTo.charAt(i))
				{
					score++;
				}
				else
				{
					score--;
				}
			}
		}
		
		return score;
	}
	
	public static List<String> GetSortedRanks(List<String> ListerCat, String AlphaWord)
	{
	
		for(int i = 1; i < ListerCat.size(); i++)
		{
			//Bubble sort I believe...
			
			if (i - 1 >= 0)
			{
				//Check if front word has a worse rank than preceding word
				if (RankWord(AlphaWord,ListerCat.get(i)) < RankWord(AlphaWord,ListerCat.get(i-1)))
				{
					String tempo = ListerCat.get(i);
					ListerCat.set(i,ListerCat.get(i-1));
					ListerCat.set(i-1, tempo);
					
					// i = 5
					// i = 6
					// i = 4
					//While it does have a worse rank, move i back two and continue
					i-=2;
				}
				
				
			}
			/*
			else if (RankWord(AlphaWord,ListerCat.get(i)) < RankWord(AlphaWord,ListerCat.get(i-1)))
			{
				String Temp = ListerCat.get(i);
				ListerCat.set(i, ListerCat.get(i-1));
				ListerCat.set(i-1, Temp);
			}*/
			
			
		}
		
		return ListerCat;
	}
	
	public static List<String> GetSortedRanksEff(List<String> ListerCat, String AlphaWord)
	{
	
		/*
		//Determine min and max values inside array
		int min = RankWord(AlphaWord,ListerCat.get(0));
		int max = RankWord(AlphaWord,ListerCat.get(0));
		
		for(int i = 0; i < ListerCat.size(); i++)
		{
			if (RankWord(AlphaWord,ListerCat.get(i)) < min)
			{
				min = RankWord(AlphaWord,ListerCat.get(i));
			}
			else if (RankWord(AlphaWord,ListerCat.get(i)) > max)
			{
				max = RankWord(AlphaWord,ListerCat.get(i));
			}
		}		//Max and min found!
		*/
		
		for(int i = 1; i < ListerCat.size(); i++)
		{
			String temp = ListerCat.get(i);
			int j;
			
			for( j = i - 1; j >=0 && RankWord(AlphaWord,temp) < RankWord(AlphaWord,ListerCat.get(j)); j--)
			{
				ListerCat.set(j+1, ListerCat.get(j));
				ListerCat.set(j+1, temp);
			}

		}

		return ListerCat;
		
	}
	
	public static List<String> GetSimilarStrings(int length, int amount, String Parent)
	{
		Random R = new Random(System.currentTimeMillis());
		
		String ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyzaeiou.,?!'";
		

		String temp = "";
		
		List<String> Words = new LinkedList<String>();
		
		//Number of words
		for(int i = 0; i < amount; i++)
		{
			
			//Mutation

			int positionsub = R.nextInt(Parent.length());
						
			
			String NewParen = "";
			
			for(int j = 0; j < length; j++)
			{
				if (j == positionsub)
				{
					//New letter from alphabet there
					NewParen += ABC.charAt(R.nextInt(ABC.length()));
				}
				else
				{
					NewParen += Parent.charAt(j);
				}
			}
			
			Parent = NewParen;
		
			
			while(temp.length() < length)
			{
				int ran = Math.abs(R.nextInt(Parent.length()));
				
				temp += Parent.charAt(ran);
			}
			
			Words.add(temp);
			temp = "";
			
		}
		
		
		return Words;
	}



}
	
